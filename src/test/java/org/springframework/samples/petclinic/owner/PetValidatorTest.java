package org.springframework.samples.petclinic.owner;

import org.junit.jupiter.api.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.xmlunit.validation.ValidationResult;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

public class PetValidatorTest {
	@Test
	void testCorrectPet() {
		Pet p = new Pet();
		p.setBirthDate(LocalDate.now());
		p.setName("kitty");
		p.setType(new PetType());
		PetValidator pv = new PetValidator();
		Errors errs = new BindException(pv, "petValidator");
		pv.validate(p, errs);
		assertFalse(errs.hasFieldErrors("name"));
		assertFalse(errs.hasFieldErrors("type"));
		assertFalse(errs.hasFieldErrors("birthDate"));
	}

	@Test
	void testNamelessPet() {
		Pet p = new Pet();
		p.setBirthDate(LocalDate.now());
		p.setType(new PetType());
		PetValidator pv = new PetValidator();
		Errors errs = new BindException(p, "pet");
		pv.validate(p, errs);
		assertTrue(errs.hasFieldErrors("name"));
		assertFalse(errs.hasFieldErrors("type"));
		assertFalse(errs.hasFieldErrors("birthDate"));
	}

	@Test
	void testNameEmptyPet() {
		Pet p = new Pet();
		p.setName("");
		p.setBirthDate(LocalDate.now());
		p.setType(new PetType());
		PetValidator pv = new PetValidator();
		Errors errs = new BindException(p, "pet");
		pv.validate(p, errs);
		assertTrue(errs.hasFieldErrors("name"));
		assertFalse(errs.hasFieldErrors("type"));
		assertFalse(errs.hasFieldErrors("birthDate"));
	}

	@Test
	void testTypelessPet() {
		Pet p = new Pet();
		p.setBirthDate(LocalDate.now());
		p.setName("kitty");
		PetValidator pv = new PetValidator();
		Errors errs = new BindException(p, "pet");
		pv.validate(p, errs);
		assertFalse(errs.hasFieldErrors("name"));
		assertTrue(errs.hasFieldErrors("type"));
		assertFalse(errs.hasFieldErrors("birthDate"));
	}

	@Test
	void testOldPet() {
		Pet p = new Pet();
		p.setBirthDate(LocalDate.now());
		p.setName("kitty");
		p.setType(new PetType());
		p.setId(2);
		PetValidator pv = new PetValidator();
		Errors errs = new BindException(p, "pet");
		pv.validate(p, errs);
		assertFalse(errs.hasFieldErrors("name"));
		assertFalse(errs.hasFieldErrors("type"));
		assertFalse(errs.hasFieldErrors("birthDate"));
	}

	@Test
	void testTypelessOldPet() {
		Pet p = new Pet();
		p.setBirthDate(LocalDate.now());
		p.setName("kitty");
		p.setId(2);
		PetValidator pv = new PetValidator();
		Errors errs = new BindException(p, "pet");
		pv.validate(p, errs);
		assertFalse(errs.hasFieldErrors("name"));
		assertFalse(errs.hasFieldErrors("type"));
		assertFalse(errs.hasFieldErrors("birthDate"));
	}
}
